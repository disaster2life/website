---
title: "Volunteer For Us"
date: 2022-05-02T12:49:02+05:30
draft: false
---
If you have some spare time, you can help us with any of the following tasks:

## Non Technical

- <a href="https://translate.codeberg.org/projects/prav/main/" target="_blank">Translations</a> (with your Codeberg account)
- Social media / publicity
- Documentation
- Design posters, flyers etc
- Creating videos about our project
- Project / operations / logistics management

Please join either the [XMPP](https://join.jabber.network/#prav@conference.quicksy.im?join) or [Matrix](https://matrix.to/#/#prav:poddery.com) group (these groups are interconnected, so joining one group is sufficient) if you are interested in helping us with non-technical tasks.

## Technical
- Android app development
- System administration
- Website maintenance

For getting involved in one of these above mentioned tasks, please join our development group via [xmpp](https://join.jabber.network/#prav-devel@chat.disroot.org?join) or [Matrix](https://matrix.to/#/#prav-devel:poddery.com). The easiest way to join is to install Quicksy in Android, clicking on the URL in the Quicksy app. If you using iOS, check out our [blog post](/blog/xmpp-apps-for-ios/) on the same. In desktop, you can use Monal IM in MacOS, Gajim in Windows, and Dino in GNU/Linux distributions to join the group.
